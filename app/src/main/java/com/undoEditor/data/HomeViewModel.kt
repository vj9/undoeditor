package com.undoEditor.data

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.undoeditor.R
import com.undoEditor.utils.Utils.countWords
import com.undoEditor.utils.PrefManager

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    val text: MutableLiveData<String> = MutableLiveData()
    val wordText: MutableLiveData<String> = MutableLiveData()
    val undoAvailable: MutableLiveData<Boolean> = MutableLiveData()
    private val editorHistory = UndoHistory()

    init {
        val text = PrefManager.getText(getApplication())
        this.text.value = text
        if (text.isNotEmpty()) {
            editorHistory.push(EditorState(text))
            undoAvailable.value = editorHistory.hasData()
        }
    }

    fun update() {
        val text = this.text.value ?: ""
        val lastState = editorHistory.getLastState()
        if ((lastState == null && text.isNullOrEmpty()) || text == lastState?.text) {
            return
        }
        editorHistory.push(EditorState(text))
        undoAvailable.value = true
        save()
        setWordCount(text)
    }

    fun setWordCount(string: String) {
        wordText.value = (getApplication() as Context).getString(
            R.string.no_of_words,
            countWords(string),
            editorHistory.list.count()
        )
    }

    fun undo() {
        if (editorHistory.hasData()) {
            val state = editorHistory.pop()
        }
        if (editorHistory.hasData()) {
            text.value = editorHistory.getLastState()?.text
        } else {
            text.value = ""
        }
        undoAvailable.value = editorHistory.list.isNotEmpty()
        // TODO: Save needs to work on observing the list variable
        save()
    }

    // TODO: Store Edit UndoHistory
    // TODO: Move prefs change to a background thread
    private fun save() {
        var value = text.value
        if (value.isNullOrEmpty()) {
            value = ""
        }
        PrefManager.putValue(getApplication(), value.toString())
    }

    override fun onCleared() {
        save()
        super.onCleared()
    }
}
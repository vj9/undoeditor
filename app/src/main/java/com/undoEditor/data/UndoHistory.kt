package com.undoEditor.data

class EditorState(val text: String? = null)

class UndoHistory {
    val list: ArrayList<EditorState> = ArrayList()

    fun push(state: EditorState) {
        list.add(state)
    }

    fun pop() {
        val lastState = getLastState()
        list.remove(lastState)
    }

    fun  getLastState(): EditorState? {
        val lastIndex = list.size - 1
        if (lastIndex == -1) return null
        return list[lastIndex]
    }

    fun hasData(): Boolean {
        return list.isNotEmpty()
    }
}
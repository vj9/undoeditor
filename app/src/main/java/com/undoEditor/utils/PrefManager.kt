package com.undoEditor.utils

import android.content.Context
import android.content.SharedPreferences

open class PrefManager {
    companion object {
        private const val PREF_NAME = "user"
        private const val PREF_KEY_TEXT = "text"

        private fun getPrefs(context: Context): SharedPreferences? {
            return context.getSharedPreferences(
                PREF_NAME,
                Context.MODE_PRIVATE
            )
        }

        private fun getEditor(context: Context): SharedPreferences.Editor? {
            val prefs =
                getPrefs(context)
            return prefs?.edit()
        }

        fun putValue(context: Context, value: String) {
            try {
                val editor = getEditor(
                    context
                ) ?: return
                editor.putString(PREF_KEY_TEXT, value)
                editor.apply()
            } catch (e: Exception) {
                // TODO: Handle exception
            }
        }

        @JvmOverloads
        fun getText(context: Context, defaultValue: String? = null): String {
            val prefs =
                getPrefs(context)
            val value = prefs?.getString(PREF_KEY_TEXT, defaultValue)
            return if (value.isNullOrEmpty()) "" else value
        }

    }
}

package com.undoEditor.utils

// TODO: Better word counter: multi locale solutions
object Utils {
    fun countWords(string: String): Int {
        if (string.trim().isNullOrEmpty()) {
            return 0
        }
        return string.trim().split("\\s".toRegex()).size
    }
}
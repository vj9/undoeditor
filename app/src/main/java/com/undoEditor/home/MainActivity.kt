package com.undoEditor.home

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.undoEditor.data.EditorState
import com.undoeditor.R
import com.undoeditor.databinding.ActivityMainBinding
import com.undoEditor.data.HomeViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createBinding()
    }

    private fun createBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.homeViewModel = homeViewModel
        binding.sinEditor.setOnFocusChangeListener { view, b ->
            if (!b) {
                textHasLostFocus()
                val imm =
                    this.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
        binding.undoButton.setOnClickListener {
            homeViewModel.undo()
        }
        binding.root.setOnClickListener {
            binding.sinEditor.clearFocus()
        }
        homeViewModel.text.observe(this, Observer<String> { text ->
            homeViewModel.setWordCount(text)
        })
    }

    private fun textHasLostFocus() {
        homeViewModel.update()
    }
}

// TODO: Proguard Rules
// TODO: Test Cases
// TODO: Associate word count and history change with text change
// TODO: Save history